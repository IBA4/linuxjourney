# cp (Copy)



Let’s start making some copies of these files. Much like copy and pasting files in other operating systems, the shell gives us an even simpler way of doing that.
```
cp mycoolfile /home/pete/Documents/cooldocs
```

mycoolfile is the file you want to copy and /home/pete/Documents/cooldocs is where you are copying the file to.

You can copy multiple files and directories as well as use wildcards. A wildcard is a character that can be substituted for a pattern based selection, giving you more flexibility with searches. You can use wildcards in every command for more flexibility.

<ul>
<li>* the wildcard of wildcards, it's used to represent all single characters or any string.</li>
<li>? used to represent one character</li>
<li>[] used to represent any character within the brackets</li>
</ul>
```
cp *.jpg /home/pete/Pictures
```
