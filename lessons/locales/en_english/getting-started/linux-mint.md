# Linux Mint

<b>Overview</b>
Linux Mint is based off of Ubuntu. It uses Ubuntu’s software repositories so the same packages are available on both distributions. Linux Mint is preferred by others over Ubuntu because it uses a different and easier to use desktop environment.

<b>Package Management</b>
Since Linux Mint is Ubuntu based, it uses the Debian package manager.

<b>Configurability</b>
Great user interface, great for beginners and less bloated than Ubuntu.

<b>Uses</b>
Great for desktop and laptop.
